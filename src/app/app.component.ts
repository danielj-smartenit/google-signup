import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { UserPage } from '../pages/user/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any }>;

  constructor(
    public googlePlus: GooglePlus,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    this.pages = [
      { title: 'Login', component: LoginPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.checkGoogleLogin();
    });
  }

  checkGoogleLogin() {
    this.googlePlus.trySilentLogin({
      'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'webClientId': '74909968696-tf0d5mocfvdphkk41d7jmnd8u420fnvd.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'offline': true
    })
      .then((data) => {
        this.nav.push(UserPage);
        this.splashScreen.hide();
      }, (error) => {
        this.nav.push(LoginPage);
        this.splashScreen.hide();
      });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
